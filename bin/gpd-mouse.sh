#!/bin/bash

# Enable mouse keys?
xkbset m

# map menu key to right mouse
# https://superuser.com/questions/336674/emulating-mouse-click-with-a-keyboard
xmodmap -e "keycode 135 = Pointer_Button2"

# map right mouse button to middle mouse button
# https://unix.stackexchange.com/questions/192191/remap-mouse-button-8-browser-forward-to-mouse-button-2-middle-click-on-r
xmodmap -e 'pointer = 1 3 2'
